import numpy as np
import matplotlib.pyplot as plt
import os
from numpy import genfromtxt
import scipy.stats as stats
from scipy.special import gamma, factorial

plt.rcParams.update({
    "figure.facecolor":  (1.0, 1.0, 1.0, 0.),  
    "axes.facecolor":    (1.0, 1.0, 1.0, 0.),  
    "savefig.facecolor": (1.0, 1.0, 1.0, 0.),
})


################################
################################ data
################################


N = 1000 

y= np.arange(0,0.5,0.001)


#######################
M2 = [3,4,6,9,15]
col = ["C0","C1","C2","C3","C4","C5","C6","C7","C8","C9"]

for i in range(len(M2)):
    os.chdir(os.path.realpath(''))
    x = []
    for k in range(100):
        data = genfromtxt('trajectory_%s_M_%s.txt' % (k,M2[i]), delimiter=',')
        j = 0
        for j in range(len(data)):
            x.append(data[j])
    
    plt.hist(x,bins=np.arange(0.,0.501,5/(N)),density=True,color=col[i],alpha=0.5)
    var = (M2[i]-2)**3 / ( (2*M2[i] - 3) * N * M2[i]**2)
    plt.plot(y,stats.norm.pdf(y,1/M2[i],np.sqrt(var)),linewidth=3,color=col[i])


plt.xlim((0,0.4))
plt.tick_params(axis='both', which='major', labelsize=20, width=1, length=10)
plt.tick_params(axis='both', which='minor', labelsize=15, width=1, length=5)
plt.savefig("stat_blank.png")
plt.show()


################################
################################ data
################################


N = 500 

y= np.arange(0,0.5,0.001)
M = 12
var = (M-2)**3 / ( 2*(2*M - 3) * N * M**2)
plt.plot(y*2*N,stats.norm.pdf(y,1/M,np.sqrt(var)),linewidth=3)


# Wright
var_w = 3.7*10**(-4)
plt.plot(y*2*N,stats.norm.pdf(y,1/M,np.sqrt(var_w)),linewidth=3,linestyle='dashed',color='C1')
plt.tick_params(axis='both', which='major', labelsize=20, width=1, length=10)
plt.tick_params(axis='both', which='minor', labelsize=15, width=1, length=5)
plt.xlim((0,130))
plt.ylim((0,50))
#plt.savefig("wright_we_blank.png")
plt.show()
