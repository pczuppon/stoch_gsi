#include <iostream>
#include <numeric>
#include <algorithm>
#include <random>
#include <fstream>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>

using namespace std;

// g++ -std=c++11 trajectory.cpp `pkg-config --libs gsl` command for compiling

#define repeats 100

int RUN(int, int, int);

// random number generator
gsl_rng * r = gsl_rng_alloc (gsl_rng_mt19937);


// main script = repeats
int main(int argc,char *argv[])
{
    int N = atoi(argv[1]);      // population size (input variable)
    int init = atoi(argv[2]);   // number of initial mating types (input variable)
           
    for (int r_ind=0; r_ind<repeats; r_ind++)
    {
        gsl_rng_set(r,r_ind);                    // setting the seed
        RUN(N,init,r_ind);            // stochastic simulation
        cout << r_ind;
        cout << "\n";
    }
    
    return(0);
}

int RUN(int N, int init, int r_ind)
{
    int success = 0;
    
    ////////////////////////////////////////////////
    ////////////////////////////////////////////////    RANDOM DISTRIBUTIONS
    ////////////////////////////////////////////////    
    double gsl_ran_flat(const gsl_rng * r, double a, double b);      // a = lower bound, b= upper bound
    
    ////////////////////////////////////////////////
    ////////////////////////////////////////////////    INITIALIZATION
    ////////////////////////////////////////////////
    int generation = 0;                 // generation
    int a_types = init;                 // number of SI alleles
    int p_types = (init-1)*init/2;      // number of possible plant types
    
    int numbers[a_types][a_types];  // number of plants with a certain type
    for (int i=0; i<a_types;i++)
    {
        for (int j=0;j<a_types;j++) numbers[i][j]=0;
    }
        
    // initialize the number of diploid plants (initially init*(init-1)/2 different types)
    for (int i=0; i<a_types;i++)
    {
        for (int j=i+1; j < a_types;j++)
        {
            numbers[i][j] = round((double)(N)/(double)(p_types));
        }
    }

    // sum up to N individuals (small bias here! -- distribute equally over off-diagonal elements (a_01, a_12, ...))
    int update=0;
    int ii = 0;
    int ij = ii+1;
    while (update==0)
    {
        int tot_init=0;
        for (int i=0;i< a_types;i++)
        {
            for (int j=i+1;j<a_types;j++) tot_init += numbers[i][j];
        }
        
        if (tot_init==N) update=1;
        
        else
        {
            if (tot_init < N)
            {
                numbers[ii%a_types][ij%a_types]++;
                ii++;
                ij++;
            }
            
            else
            {
                while (numbers[ii%a_types][ij%a_types]==0) 
                {
                    if (ij%a_types==ii%a_types)
                    {
                        ii++;
                        ij = ii+1;
                    }
                    else ij++;
                }
                
                numbers[ii%a_types][ij%a_types]--;
                ii++;
                ij++;
                
            }
        }
    }
    
    ////////////////////// Initialize pollen frequencies
    double normy = 2.*(double)N;                     // normalization factor for pollen
    double pollen[a_types];                         // pollen frequency  
    
    for (int i=0;i<a_types;i++) pollen[i] = 0.;
    
    for (int i = 0; i<a_types; i++)
    {
        for (int j = 0; j<i;j++)  pollen[i] += (double)numbers[j][i]/normy;
    
        for (int j = i+1; j<a_types;j++) pollen[i] += (double)numbers[i][j]/normy;
    }
    
    
    /// Initialize birth transition matrix
    double birth[a_types][a_types];                // rates of the different plants for reproduction (increase by 1)
    for (int i=0;i< a_types;i++)
    {
        for (int j=0;j<a_types;j++) birth[i][j] = 0.;
    }
    
    ofstream file ("trajectory_" + std::to_string(r_ind) + "_M_" + std::to_string(init) + ".txt", ios::app);   // file output
    
    ////////////////////////////////////////////////
    ////////////////////////////////////////////////    SIMULATION
    ////////////////////////////////////////////////
    while (generation < 1200*N)
    {   
        ///////////////////////////////////////////
        ///////////////////////////////////////////     Birth
        ///////////////////////////////////////////
        double birth_tot = 0.;
        
        for (int i=0;i<a_types;i++)
        {
            
            for (int j=i+1; j<a_types;j++)
            {
                double fert1 = 0.;
                double fert2 = 0.;
                 
                //////////////////// i pollen fertilization
                for (int k=0; k<j;k++)    
                {   
                    if (k!=i)
                    {
                        fert1 += (double)numbers[k][j] * pollen[i]/(1.-pollen[j]-pollen[k]);
                    }
                }
                              
                for (int k=j+1; k<a_types;k++) 
                {   
                    if (k!=i)
                    {
                        fert1 += (double)numbers[j][k] * pollen[i]/(1.-pollen[j]-pollen[k]);
                    }
                }
                            
                //fert1 -= (double)numbers[i][j] * pollen[i]/(1.-pollen[j]-pollen[i]);
                
                //////////////////// j pollen fertilization
                for (int k=0; k<i;k++)
                {
                    if (k!=j)
                    {
                        fert2 += (double)numbers[k][i] * pollen[j]/(1.-pollen[i]-pollen[k]);
                    }
                }
                                           
                for (int k=i+1; k<a_types;k++)    
                {
                    if (k!=j)
                    {
                        fert2 += (double)numbers[i][k] * pollen[j]/(1.-pollen[i]-pollen[k]);
                    }
                }                
                
                //fert2 -= (double)numbers[i][j] * pollen[j]/(1.-pollen[j]-pollen[i]);
                
                //////////////////// transition rate T_a_ij + 
                birth[i][j] = (fert1+fert2)*(1.-(double)numbers[i][j]/((double)N))/2.;
                birth_tot += birth[i][j];
            
            }        
        }
        
        double randbirth = gsl_ran_flat(r, 0.0, 1.0); 
        double randdeath = gsl_ran_flat(r, 0.0, 1.0); 
        
        int update=0;   // if update = 1, update happens, if = 0 no update happened
        int i = 0;      // index parameter birth
        int j = 0;      // index parameter birth
        
        double birth_up = 0;
        double death_up = 0;
        
        while(update==0)
        {
            ///////////////
            /////////////// BIRTH loop
            ///////////////
            birth_up += birth[i][j]/birth_tot;
            if (randbirth < birth_up)
            {
                ///////////////
                /////////////// DEATH loop
                ///////////////
                int k1 = 0;     // index parameter death
                int k2 = 0;     // index parameter death
               
                while(update==0)
                {
                    death_up += (double)numbers[k1][k2]/((double)N-1.);
                    if (k1==i && k2==j) death_up -= 1./((double)N-1.);      // correction that mother plant cannot be replaced!
                    
                    if (randdeath < death_up)
                    {
                        // update numbers
                        numbers[k1][k2]--;
                        numbers[i][j]++;
                        generation++;
                                             
                        update = 1;
                    }
                    
                    if (k2 < a_types-1) k2++;
                    else
                    {
                        k1++;
                        k2=k1+1;
                    }
                }
            }
            
            if (j < a_types-1) j++;
            else
            {
                i++;
                j=i+1;
            }
        }
        
        ///////////////
        /////////////// UPDATE POLLEN NUMBERS
        ///////////////
        for (int i = 0; i<a_types; i++) pollen[i] = 0.;
                             
        for (int i = 0; i<a_types; i++)
        {
            for (int j = 0; j<i;j++)  pollen[i] += (double)numbers[j][i]/normy;
        
            for (int j = i+1; j<a_types;j++) pollen[i] += (double)numbers[i][j]/normy;
            
            if (pollen[i] < 1./(2.*(double)N)) pollen[i] = 0.;
        }
     
        // Write data
                
        if (generation%N == 0 && generation > 200*N)
        {
            file << pollen[1];
            file << "\n";
        }       
    }
    
    file.close();
    
    return(0);   
}
