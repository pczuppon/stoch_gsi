import numpy as np
import matplotlib.pyplot as plt
from numpy import genfromtxt
from mpl_toolkits.mplot3d import Axes3D
import scipy.stats as stats
from scipy.optimize import minimize
from scipy.optimize import fsolve
from scipy.stats import norm
from scipy.stats import multivariate_hypergeom

plt.rcParams.update({
    "figure.facecolor":  (1.0, 1.0, 1.0, 0.),  
    "axes.facecolor":    (1.0, 1.0, 1.0, 0.),  
    "savefig.facecolor": (1.0, 1.0, 1.0, 0.),
})


######################
###################### estimate from data sets
######################
init = np.array([20,100])
init2 = np.array([100])

M = [15,20,25] #[5,10,15] #[15,20,25]
N = 3000
sample_size = [50,100,200]
draws = 50

M_est = np.zeros((len(sample_size),len(M),draws))
N_est = np.zeros((len(sample_size),len(M),draws))

for k in range(len(M)):
    print("M= ", M[k])
    
    ###################### simulated data with M=10, N=1000
    data = genfromtxt('M_%s_N_%s.txt' % (M[k],N), delimiter=',')
    data_matrix = np.zeros((M[k],M[k]))
    
    ind_low = 0
    for i in range(M[k]):
        data_matrix[i,(i+1):] = data[ind_low:(ind_low+(M[k]-(i+1)))]
        ind_low = ind_low + M[k]-(i+1)
    
    data_pollen = np.zeros(M[k])
    
    for i in range(M[k]):
        j = 0
        while (j < i):
            data_pollen[i] += data_matrix[j,i]
            j += 1
        
        if (j == i):
            j += 1
        
        while (j > i and j < M[k]):
            data_pollen[i] += data_matrix[i,j]
            j += 1
    
    for m in range(len(sample_size)):
        print("sample size = ", sample_size[m])
        for l in range(draws):
            print(l)
            ################ draw a random sample from the data
            sample_matrix = np.zeros((M[k],M[k]))
            
            data_sample = multivariate_hypergeom.rvs(m=data[0:-1].astype(int), n=sample_size[m],size = 1)[0]      
            
            ind_low = 0
            for i in range(M[k]-1):
                sample_matrix[i,(i+1):] = data_sample[ind_low:(ind_low+(M[k]-(i+1)))]
                ind_low = ind_low + M[k]-(i+1)
            
            
            sample_pollen = np.zeros(M[k])
            for i in range(M[k]):
                j = 0
                while (j < i):
                    sample_pollen[i] += sample_matrix[j,i]
                    j += 1
                
                if (j == i):
                    j += 1
                
                while (j > i and j < M[k]):
                    sample_pollen[i] += sample_matrix[i,j]
                    j += 1
            
            sample_pollen = sample_pollen
            
            ###################
            ############ define data
            ###################
            n = sample_size[m]
            s_vector = sample_pollen
            
            s_vector = s_vector[s_vector > 0]
            
            ###################
            ############ fit a normal distribution
            ###################            
            Mhat = 1/norm.fit(s_vector/(2*sample_size[m]))[0]
            M_est[m,k,l] = Mhat
            N_est[m,k,l] = (Mhat-2)**3/(norm.fit(s_vector/(2*sample_size[m]))[1]**2 * Mhat**2 * (2*Mhat - 3))   
            
            print("M est = ", Mhat)
            print("N est = ", N_est[m,k,l])

p1 = plt.violinplot(N_est[0,0,:],[14]) 
p2 = plt.violinplot(N_est[0,1,:],[19])
p3 = plt.violinplot(N_est[0,2,:],[24])

p4 = plt.violinplot(N_est[1,0,:],[15]) 
p5 = plt.violinplot(N_est[1,1,:],[20])
p6 = plt.violinplot(N_est[1,2,:],[25])

p7 = plt.violinplot(N_est[2,0,:],[16])
p8 = plt.violinplot(N_est[2,1,:],[21])
p9 = plt.violinplot(N_est[2,2,:],[26])

for pc in p2['bodies']:
    pc.set_facecolor('C0')
    pc.set_edgecolor('C0')

for pc in p3['bodies']:
    pc.set_facecolor('C0')
    pc.set_edgecolor('C0')

for pc in p4['bodies']:
    pc.set_facecolor('C1')
    pc.set_edgecolor('C1')

for pc in p5['bodies']:
    pc.set_facecolor('C1')
    pc.set_edgecolor('C1')

for pc in p6['bodies']:
    pc.set_facecolor('C1')
    pc.set_edgecolor('C1')

for pc in p7['bodies']:
    pc.set_facecolor('C2')
    pc.set_edgecolor('C2')

for pc in p8['bodies']:
    pc.set_facecolor('C2')
    pc.set_edgecolor('C2')

for pc in p9['bodies']:
    pc.set_facecolor('C2')
    pc.set_edgecolor('C2')


for partname in ('cbars','cmins','cmaxes'):
    p1[partname].set_edgecolor('C0')
    p1[partname].set_linewidth(2)
    p2[partname].set_edgecolor('C0')
    p2[partname].set_linewidth(2)
    p3[partname].set_edgecolor('C0')
    p3[partname].set_linewidth(2)
    p4[partname].set_edgecolor('C1')
    p4[partname].set_linewidth(2)
    p5[partname].set_edgecolor('C1')
    p5[partname].set_linewidth(2)
    p6[partname].set_edgecolor('C1')
    p6[partname].set_linewidth(2)
    p7[partname].set_edgecolor('C2')
    p7[partname].set_linewidth(2)
    p8[partname].set_edgecolor('C2')
    p8[partname].set_linewidth(2)
    p9[partname].set_edgecolor('C2')
    p9[partname].set_linewidth(2)

xplot = np.arange(14,26.01,0.01)
plt.plot(xplot,[N]*len(xplot),color='black',linewidth=3,linestyle='dashed')
plt.ylim((0,6000))
plt.tick_params(axis='both', which='major', labelsize=20, width=1, length=10)
plt.tick_params(axis='both', which='minor', labelsize=15, width=1, length=5)
plt.show()





