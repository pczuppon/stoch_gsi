import numpy as np
import matplotlib.pyplot as plt
from numpy import genfromtxt
from mpl_toolkits.mplot3d import Axes3D
import scipy.stats as stats
from scipy.optimize import minimize
from scipy.optimize import fsolve
from scipy.stats import norm
from scipy.stats import multivariate_hypergeom

plt.rcParams.update({
    "figure.facecolor":  (1.0, 1.0, 1.0, 0.),  
    "axes.facecolor":    (1.0, 1.0, 1.0, 0.),  
    "savefig.facecolor": (1.0, 1.0, 1.0, 0.),
})

###################
############ define integral of the expectation Eq. (13) left hand side
###################
def integration0(pars):
    M = pars
    N = 500
    dx = 0.0005
    mean = 2./M
    var = 4*(M-2)**3/(N * M**2 * (2*M-3))
    x = np.arange(dx,1.,dx)
    result = 0.
    for i in range(len(x)):
        result += n*x[i]/(1.-(1.-x[i])**n) * dx * norm.pdf(x[i],mean,np.sqrt(var))
    
    return result


def integration(pars):
    M,N = pars
    dx = 0.0005
    mean = 2./M
    var = 4*(M-2)**3/(N * M**2 * (2*M-3))
    x = np.arange(dx,1.,dx)
    result = 0.
    for i in range(len(x)):
        result += n*x[i]/(1.-(1.-x[i])**n) * dx * norm.pdf(x[i],mean,np.sqrt(var))
    
    return result


###################
############ define integral of the expectation of the variance Eq. (15a)
###################
def integration2(pars):
    M,N = pars
    #M=41
    dx = 0.0005
    mean = 2./M
    var = 4*(M-2)**3/(N * M**2 * (2*M-3))
    x = np.arange(dx,1.,dx)
    result = 0.
    for i in range(len(x)):
        result += ((n*x[i]*(1.-x[i]))/(1.-(1.-x[i])**n) - n**2 * x[i]**2 * (1-x[i])**n/((1-(1-x[i])**n)**2)) * dx * norm.pdf(x[i],mean,np.sqrt(var))
    
    return result


###################
############ define integral of the variance of the expectatoin Eq. (16)
###################
def integration3(pars):
    M,N = pars
    #M = 41
    dx = 0.0005
    mean = 2./M
    var = 4*(M-2)**3/(N * M**2 * (2*M-3))
    x = np.arange(dx,1.,dx)
    result = 0.
    for i in range(len(x)):
        result += n**2 *x[i]**2 /(1.-(1.-x[i])**n)**2 * dx * norm.pdf(x[i],mean,np.sqrt(var))
    
    #pars = np.array([M,N])
    result -= integration(pars)**2
    
    return result

######################
###################### estimate from data sets
######################
init = np.array([20,100])
init2 = np.array([100])

M = [20]
N = 3000
sample_size = [100]
draws = 1

M_est = np.zeros((len(sample_size),len(M),draws))
N_est = np.zeros((len(sample_size),len(M),draws))

for k in range(len(M)):
    print("M= ", M[k])
    
    ###################### simulated data with M=10, N=1000
    data = genfromtxt('M_%s_N_3000.txt' % (M[k]), delimiter=',')
    data_matrix = np.zeros((M[k],M[k]))
    
    ind_low = 0
    for i in range(M[k]):
        data_matrix[i,(i+1):] = data[ind_low:(ind_low+(M[k]-(i+1)))]
        ind_low = ind_low + M[k]-(i+1)
    
    data_pollen = np.zeros(M[k])
    
    for i in range(M[k]):
        j = 0
        while (j < i):
            data_pollen[i] += data_matrix[j,i]
            j += 1
        
        if (j == i):
            j += 1
        
        while (j > i and j < M[k]):
            data_pollen[i] += data_matrix[i,j]
            j += 1
    
    for m in range(len(sample_size)):
        print("sample size = ", sample_size[m])
        for l in range(draws):
            print(l)
            ################ draw a random sample from the data
            sample_matrix = np.zeros((M[k],M[k]))
            
            data_sample = multivariate_hypergeom.rvs(m=data[0:-1].astype(int), n=sample_size[m],size = 1)[0]      
            
            ind_low = 0
            for i in range(M[k]-1):
                sample_matrix[i,(i+1):] = data_sample[ind_low:(ind_low+(M[k]-(i+1)))]
                ind_low = ind_low + M[k]-(i+1)
            
            
            sample_pollen = np.zeros(M[k])
            for i in range(M[k]):
                j = 0
                while (j < i):
                    sample_pollen[i] += sample_matrix[j,i]
                    j += 1
                
                if (j == i):
                    j += 1
                
                while (j > i and j < M[k]):
                    sample_pollen[i] += sample_matrix[i,j]
                    j += 1
            
            sample_pollen = sample_pollen
            
            ###################
            ############ define data
            ###################
            n = sample_size[m]
            s_vector = sample_pollen
            
            s_vector = s_vector[s_vector > 0]            
            



def root_function(x):
    s_vector = sample_pollen
    return (((np.mean(s_vector[np.where(s_vector>0)[0]]) - integration(x))**2)/np.mean(s_vector[np.where(s_vector>0)[0]]) + ((np.var(s_vector[np.where(s_vector>0)[0]])-integration2(x)-integration3(x))**2)/np.var(s_vector[np.where(s_vector>0)[0]]))


res = minimize(root_function,init,method='nelder-mead',options={'disp': True}).x

x = np.arange(5, 50)
y = np.arange(100, 5000)
xgrid, ygrid = np.meshgrid(x, y)
xy = np.stack([xgrid, ygrid])

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.view_init(45, -45)
ax.plot_surface(xgrid, ygrid, root_function(xy), cmap='terrain')
ax.set_xlabel('M')
ax.set_ylabel('N')
plt.show()



