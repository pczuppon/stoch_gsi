import numpy as np
import matplotlib.pyplot as plt
import os
from numpy import genfromtxt
import scipy.stats as stats
from scipy.special import gamma, factorial

plt.rcParams.update({
    "figure.facecolor":  (1.0, 1.0, 1.0, 0.),  
    "axes.facecolor":    (1.0, 1.0, 1.0, 0.),  
    "savefig.facecolor": (1.0, 1.0, 1.0, 0.),
})

################################
################################ data
################################
N = 1000 
y= np.arange(0,0.5,0.001)

####################### Plot stationary distribution
M2 = np.arange(3,8,1)
col = ["C0","C1","C2","C3","C4"]

for i in range(len(M2)):
    os.chdir(os.path.realpath(''))
    data = genfromtxt('trajectory_M_%s.txt' % (M2[i]), delimiter=',')
    
    plt.hist(data,bins=np.arange(0.,0.501,5/N),density=True,color=col[i],alpha=0.5)
    if (M2[i]> 3): 
        var1 = 2*(M2[i]-2)*(M2[i]+1) / (N* M2[i]**2 * (M2[i]-1)**2)                                          # without cov (good)
    else:
        var1 = 4*(M2[i]-2)*(M2[i]+1) / (3*N* M2[i]**2 * (M2[i]-1)**2)  
    plt.plot(y,stats.norm.pdf(y,2/(M2[i]*(M2[i]-1)),np.sqrt(var1)),linewidth=3,color=col[i])

plt.tick_params(axis='both', which='major', labelsize=20, width=1, length=10)
plt.tick_params(axis='both', which='minor', labelsize=15, width=1, length=5)
plt.savefig("plant_stat_blank.png")
plt.show()



####################### Plot breakdown
#data = genfromtxt('trajectory_M_15.txt', delimiter=',')
#plt.hist(data,bins=np.arange(0.,0.501,4/N),density=True,color='black',alpha=0.25)
#var15 = 2*(15-2)*(15+1) / (N* 15**2 * (15-1)**2)                                          # without cov (good)
#plt.plot(y,stats.norm.pdf(y,2/(15*(15-1)),np.sqrt(var1)),linewidth=3,color='black')
#plt.xlim((0,.1))
#plt.tick_params(axis='both', which='major', labelsize=20, width=1, length=10)
#plt.tick_params(axis='both', which='minor', labelsize=15, width=1, length=5)
#plt.show()
