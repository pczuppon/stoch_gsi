import numpy as np
import matplotlib.pyplot as plt
import os
from numpy import genfromtxt

################################
################################ data
################################
plt.rcParams.update({
    "figure.facecolor":  (1.0, 1.0, 1.0, 0.),  
    "axes.facecolor":    (1.0, 1.0, 1.0, 0.),  
    "savefig.facecolor": (1.0, 1.0, 1.0, 0.),
})



os.chdir(os.path.realpath(''))
data = genfromtxt('establishment.txt', delimiter=',')

################################
################################ read data
################################
repeats = 100000
prob = []
alleles = []

for i in range(len(data)):
    alleles.append(float(data[i][0]))
    prob.append(float(data[i][1]))
   

allele_plot = np.arange(3,20,0.01)
################################
################################ Plot
################################
plt.plot(allele_plot,2/(2*(allele_plot)-3),linewidth=3)
plt.plot(allele_plot,(1-np.exp(-2/(2*(allele_plot)-3))),linewidth=3)
plt.plot(allele_plot,2/allele_plot,linewidth=3)
#plt.plot(allele_plot,(1-np.exp(-2/(2*(allele_plot)-3)))/(1-np.exp(4*N/(5+(allele_plot+1)*(3-2*(allele_plot+1))))))
plt.plot(alleles,prob,'o',color='black',markersize=10)

plt.ylim((0, 1))
plt.xlim((0,10))
plt.fill_between([0,3],[1,1],color='lightgrey')
plt.tick_params(axis='both', which='major', labelsize=20, width=1, length=10)
plt.tick_params(axis='both', which='minor', labelsize=15, width=1, length=5)
#plt.savefig("invasion_pres_blank.png")
plt.show()



