import numpy as np
import matplotlib.pyplot as plt
import os
from numpy import genfromtxt
import scipy.stats as stats

plt.rcParams.update({
    "figure.facecolor":  (1.0, 1.0, 1.0, 0.),  
    "axes.facecolor":    (1.0, 1.0, 1.0, 0.),  
    "savefig.facecolor": (1.0, 1.0, 1.0, 0.),
})

################################
################################ theory
################################
N = 5000
mut = 1/10
pred = []
tplot = np.arange(1,7001,1)
tplot2 = np.arange(1,8000,1)

for i in range(len(tplot2)):
    pred.append(min(37,max(3,np.sqrt(2*mut*tplot2[i]+1)+2)))


#plt.plot(tplot,pred,color='C0',linewidth=3)
#plt.show()


################################
################################ data
################################
reps = np.arange(0,1000,1)
os.chdir(os.path.realpath(''))

datamatrix = np.zeros((len(reps),len(tplot)))

for i in range(len(reps)):
    data = genfromtxt('diverse_N_5000_ind_%s.txt' % (reps[i]), delimiter=',',invalid_raise=False)
    datamatrix[i,:] = data[0:7000,1]    


means = np.zeros(len(tplot))
upper = np.zeros(len(tplot))
lower = np.zeros(len(tplot))

for j in range(len(tplot)):
    means[j] = np.mean(datamatrix[:,j])
    upper[j] = np.percentile(datamatrix[:,j],75)
    lower[j] = np.percentile(datamatrix[:,j],25)


plt.plot(tplot2,pred,color='C0',linewidth=3)
plt.plot(tplot,means,color='black',lw=2)
plt.plot(tplot,upper,ls='dashed',color='black',lw=1)
plt.plot(tplot,lower,ls='dashed',color='black',lw=1)
plt.ylim((0,40))
plt.tick_params(axis='both', which='major', labelsize=20, width=1, length=10)
plt.tick_params(axis='both', which='minor', labelsize=15, width=1, length=5)
plt.show()

