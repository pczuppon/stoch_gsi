# Stochastic model of GSI

Code to reproduce the figures of the manuscript "Revisiting the number of self-incompatibility alleles in finite populations: from old models to new results"

C++ files in the folder contain the compiling command at the beginning of the file. The produced data can then be processed by the python scripts in the respective folders to reproduce the figures.
