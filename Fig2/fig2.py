import numpy as np
import matplotlib.pyplot as plt
import os
from numpy import genfromtxt
import scipy.stats as stats

plt.rcParams.update({
    "figure.facecolor":  (1.0, 1.0, 1.0, 0.),  
    "axes.facecolor":    (1.0, 1.0, 1.0, 0.),  
    "savefig.facecolor": (1.0, 1.0, 1.0, 0.),
})

################################
################################ theory
################################
N = np.arange(10,6000,10)
pred = []
pred_W = []
M = 4

for i in range(len(N)):
    print(i)
    update = 0
    mut = 1/(100*N[i]) 
    
    while (update == 0):
        var = (M-2)**3/(N[i] * M**2 * (2*M - 3)) 
        sol = M/(N[i]) * stats.norm.cdf(1/(2*N[i]),1/M,np.sqrt(var))     
        if (mut < sol):
            update = 1
            pred.append(M-1)
        
        else:
            M += 1


plt.plot(N,pred,color='C0',linewidth=3)

################################
################################ data
################################
Nplot = [50,100,500,1000,2000,3000,4000,5000]
init = 30
os.chdir(os.path.realpath(''))

for i in range(len(Nplot)):
    data = genfromtxt('numbers_N_%s_init_%s.txt' % (Nplot[i],init), delimiter=',')
    plt.plot(Nplot[i],np.mean(data),'o',color='black',markersize=10)

plt.ylim((0,40))
plt.tick_params(axis='both', which='major', labelsize=20, width=1, length=10)
plt.tick_params(axis='both', which='minor', labelsize=15, width=1, length=5)
plt.savefig("fig2_blank.png")
plt.show()

