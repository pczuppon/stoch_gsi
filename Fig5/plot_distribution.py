import numpy as np
import matplotlib.pyplot as plt
import os
from numpy import genfromtxt
import scipy.stats as stats
from scipy.special import gamma, factorial

################################
################################ data
################################
plt.rcParams.update({
    "figure.facecolor":  (1.0, 1.0, 1.0, 0.),  
    "axes.facecolor":    (1.0, 1.0, 1.0, 0.),  
    "savefig.facecolor": (1.0, 1.0, 1.0, 0.),
})

N = 500
M = 12
y = np.arange(0,0.5,0.001)


#######################
######## N = 500
var = (M-2)**3 / ( 2*(2*M - 3) * N * M**2)
var_wf = 3.7*10**(-4)

plt.plot(y,stats.norm.pdf(y,1/M,np.sqrt(var)),linewidth=3,color='C0')

plt.plot(y,stats.norm.pdf(y,1/M,np.sqrt(var_wf)),linewidth=3,color='black',linestyle='dashed')

plt.xlim((0,0.2))
plt.tick_params(axis='both', which='major', labelsize=20, width=1, length=10)
plt.tick_params(axis='both', which='minor', labelsize=15, width=1, length=5)
#plt.savefig("comp_wright_blank.png")
#plt.show()


#########################
######## N = 50

N = 50
M = 6
y = np.arange(0,0.5,0.001)


#######################
var = (M-2)**3 / ( 2*(2*M - 3) * N * M**2)
var_wf = 0.00251

plt.plot(y,stats.norm.pdf(y,1/M,np.sqrt(var)),linewidth=3,color='C0')

plt.plot(y,stats.norm.pdf(y,1/M,np.sqrt(var_wf)),linewidth=3,color='black',linestyle='dashed')

plt.xlim((0,0.4))
plt.savefig("comp_wright_blank.png")
plt.show()
#plt.show()
